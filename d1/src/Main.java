import java.util.Scanner;

public class Main {
    /*
        Main Class

        The Main class is the entry point for our Java program. It is responsible for executing our code. The main class usually has one method inside it, the main() method. The main method is the method to run our code.

        ctrl + / - single comments
        ctrl + shift + / - multi-line comments
    */
        public static void main(String[] args){
            /*
                Main Method is where most executable code is applied to.

                "public" is an access modifier which simply tells the application which classes have access to our method/attributes.

                "static" means that the method/property belongs to the class. This means that it is accessible without having to create an instance of a class.

                "void" means that this method will not return data. Because in Java, we have to declare the data type of the method's return. And since main does not return data, we add "void".
            */
            //System.out.println() is a statement which will allow us to print the value of the argument passed into it in our terminal.
            System.out.println("Listher Gonzles");
            System.out.println("I am learning Java!");

            //In Java, to be able to declare a variable, we have to identify or declare its data type. Which means that the variable will expect and only accept data with the data type declared.

            int myNum;
            myNum = 3;
            System.out.println(myNum);

            //myNum = "sampleString";

            myNum = 2500;
            System.out.println(myNum);

            int nationalPopulation = 2147483647;
            System.out.println(nationalPopulation);

            //nationalPopulation = 2147483648;

            //L is added at the end of a long number to be recognized as long. Otherwise, it will be recognized as int.
            long worldPopulation = 7862881145L;
            System.out.println(worldPopulation);

            //f is added at the end of a float to be recognized as float.
            float piFloat = 3.141592f;
            System.out.println(piFloat);

            double piDouble = 3.1415926;
            System.out.println(piDouble);

            //char - can only hold one character
            char letter = 'a';
            System.out.println(letter);

            //"" is a literal for string
            //letter = "b"

            //boolean
            boolean isMVP = true;
            boolean isChampion = false;
            System.out.println(isMVP);
            System.out.println(isChampion);

            //constants in Java is declared with final keyword and the data type
            //final dataType VARIABLENAME
            final int PRINCIPAL = 3000;
            //PRINCIPAL = 4000;
            System.out.println(PRINCIPAL);

            //String in Java is a non-primitive data type. This is because strings are actually objects that can use methods.
            //Non-primitive data types have access to methods:
            //Array, Class, Interface

            String username = "yusukeUrameshi91";
            System.out.println(username);

            //String username2 = 'a';

            //Strings, being non-primitive, have access to methods.
            //isEmpty() is a string method which returns boolean. It will check the length of the string and returns true if length is 0. Returns false if otherwise.
            System.out.println(username.isEmpty());

            String username2 = "";
            System.out.println(username2.isEmpty());

            //Scanner is a class in Java which allows us to create an instance that accepts input from the terminal. It's like prompt() in JS.
            //However, Scanner being a class pre-defined by Java, has to be imported to be used.
            Scanner scannerName = new Scanner(System.in);

            //.nextLine() receives user input and returns a string.
            System.out.println("What is your name?");
            String myName = scannerName.nextLine();

            System.out.println("Your name is " + myName + "!");

            //.nextInt() receives user input and returns an integer.
            System.out.println("What is your favorite number?");
            int myFaveNum = scannerName.nextInt();

            System.out.println("Your favorite number is " + myFaveNum + "!");

            //.nextDouble() receives user input and returns a double.
            System.out.println("What was your average grade in HS?");
            double aveGrade = scannerName.nextDouble();

            System.out.println("Your average grade in HS was " + aveGrade + "!");

            //Much like in JS, Java also has similar mathematical operators
            System.out.println("Enter the first number:");
            int num1 = scannerName.nextInt();

            System.out.println("Enter the second number:");
            int num2 = scannerName.nextInt();

            int sum = num1 + num2;
            System.out.println("The sum of both numbers is " + sum);

            /* Mini Activity*/
            System.out.println("Enter the first number:");
            int num3 = scannerName.nextInt();

            System.out.println("Enter the second number:");
            int num4 = scannerName.nextInt();

            int diff = num3 - num4;
            System.out.println("The difference between the numbers is " + diff);
        }
}