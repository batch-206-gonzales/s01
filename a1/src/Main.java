import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scannerName = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName =scannerName.nextLine();

        System.out.println("Last Name:");
        String lastName =scannerName.nextLine();

        System.out.println("First Subject Grade:");
        double firstGrade =scannerName.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondGrade =scannerName.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdGrade =scannerName.nextDouble();

        double aveGrade = (firstGrade + secondGrade + thirdGrade)/3;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + aveGrade);
    }
}